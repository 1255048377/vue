import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from "@/views/LoginView.vue";
import Layout from "@/views/Layout.vue";
import RegisterView from "@/views/RegisterView.vue";
import BillView from "@/views/InComeView.vue";
import InComeView from "@/views/InComeView.vue";
import PayoutView from "@/views/PayoutView.vue";
import ForumView from "@/views/ForumView.vue";
import PayoutDataView from "@/views/PayoutDataView.vue";
import IncomeDataView from "@/views/IncomeDataView.vue";

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: LoginView
  },
  {
    path: '/register',
    name: 'Register',
    component: RegisterView
  },
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    children:[
      {
        path: '',
        name: 'home',
        component: HomeView
      },
      {
        path: 'income',
        name: 'income',
        component: InComeView
      },
      {
        path: 'payout',
        name: 'payout',
        component: PayoutView
      },
      {
        path: 'payoutdata',
        name: 'payoutdata',
        component: PayoutDataView
      },
      {
        path: 'incomedata',
        name: 'incomedata',
        component: IncomeDataView
      },
      {
        path: 'forum',
        name: 'Forum',
        component: ForumView
      },
      {
        path: 'user',
        name: 'user',
        component: () => import('../views/UserView.vue')
      }
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

//路由守卫
// router.beforeEach((to,from,next) =>{
//   if(to.path ==='/login'){
//     next();
//   }
//   if(to.path ==='/register'){
//     next();
//   }
//   const user = localStorage.getItem("user");
//   if(!user){
//     if(to.path ==='register')
//     {
//       next();
//     }else{
//       return next('/login')
//     }
//   }
//   next();
// })

export default router
