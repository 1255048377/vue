package com.example.entity;

public class Parmas {

    private String name;
    private String sex;
    private String bookname;
    private String inoutclassify;
    private String billclassify;
    private Integer pageNum;
    private Integer pageSize;
    private String uname;

    public Parmas(String uname) {
        this.uname = uname;
    }
    public Parmas() {
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getInoutclassify() {
        return inoutclassify;
    }

    public void setInoutclassify(String inoutclassify) {
        this.inoutclassify = inoutclassify;
    }

    public String getBillclassify() {
        return billclassify;
    }

    public void setBillclassify(String billclassify) {
        this.billclassify = billclassify;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
