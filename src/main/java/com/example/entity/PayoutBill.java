package com.example.entity;

import javax.persistence.*;

@Table(name = "bill")
public class PayoutBill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "bookname")
    private String bookname;
    @Column(name = "inoutclassify")
    private String inoutclassify;

    @Column(name = "sum")
    private Integer sum;
    @Column(name = "billclassify")
    private String billclassify;
    @Column(name = "account")
    private Double account;
    @Column(name = "notes")
    private String notes;
    @Column(name = "uname")
    private String uname;

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getInoutclassify() {
        return inoutclassify;
    }

    public void setInoutclassify(String inoutclassify) {
        this.inoutclassify = inoutclassify;
    }

    public String getBillclassify() {
        return billclassify;
    }

    public void setBillclassify(String billclassify) {
        this.billclassify = billclassify;
    }

    public Double getAccount() {
        return account;
    }

    public void setAccount(Double account) {
        this.account = account;
    }
}
