package com.example.dao;

import com.example.entity.Parmas;
import com.example.entity.User;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Repository
public interface UserDao extends Mapper<User> {

    // 1. 基于注解的方式@Select("select * from user")
//    List<User> getUser();

    List<User> findBySearch(@Param("params") Parmas params);

    @Select("select * from user where name = #{name} limit 1")
    User findByName(@Param("name") String name);

    @Select("select * from user where name = #{name} and password = #{password} limit 1")
    User findByNameAndPassword(@Param("name") String name, @Param("password") String password);
}