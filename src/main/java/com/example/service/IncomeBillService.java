package com.example.service;



import com.example.dao.IncomeBillDao;
import com.example.entity.IncomeBill;
import com.example.entity.Parmas;
import com.example.entity.PayoutBill;
import com.example.entity.User;
import com.example.exception.CustomException;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.List;


@Service
public class IncomeBillService {

    @Resource
    private IncomeBillDao incomeBillDao;


    public PageInfo<IncomeBill> findBySearch(Parmas params) {
        //开启分页查询
        PageHelper.startPage(params.getPageNum(), params.getPageSize());
        List<IncomeBill> list = incomeBillDao.findBySearch(params);
        return PageInfo.of(list);
    }

//    public void add(IncomeBill incomeBill) {
//        incomeBillDao.insertSelective(incomeBill);
//    }

    public void add(IncomeBill incomeBill) {
        if(incomeBill.getInoutclassify()== null)
            incomeBill.setInoutclassify("收入");
        incomeBillDao.insertSelective(incomeBill);
    }

    public void update(IncomeBill incomeBill) {
        incomeBillDao.updateByPrimaryKeySelective(incomeBill);
    }

    public void delete(Integer id) {
        incomeBillDao.deleteByPrimaryKey(id);
    }



    public List<IncomeBill> getIncomeBill(String uname) {
        return incomeBillDao.getIncomeBill(uname);
    }

    public List<IncomeBill> findAll(Parmas params) {
        return incomeBillDao.findAll(params);
    }


}