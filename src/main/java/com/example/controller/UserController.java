package com.example.controller;

import com.example.common.Result;
import com.example.entity.Parmas;
import com.example.entity.User;
import com.example.service.UserService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @GetMapping
    public Result getUser() {
        List<User> list=userService.getUser();
        return Result.success(list);
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id){
        userService.delete(id);
        return Result.success();
    }

    @PostMapping("/login")
    public Result login(@RequestBody User user){
        User loginUser = userService.login(user);
        return Result.success(loginUser);
    }

    @PostMapping("/register")
    public Result register(@RequestBody User user){
        userService.register(user);
        return Result.success();
    }


    @PostMapping
    public Result save(@RequestBody User user) {
        if(user.getId() == null) {
            userService.add(user);
        }else{
            userService.upadte(user);
        }
        return Result.success();
    }


    @GetMapping("/search")
    public Result findBySearch(Parmas params) {
        PageInfo<User> info = userService.findBySearch(params);
        return Result.success(info);
    }

}