package com.example.controller;


import cn.hutool.core.util.ObjectUtil;
import com.example.common.Result;
import com.example.entity.IncomeBill;
import com.example.entity.Parmas;
import com.example.entity.PayoutBill;
import com.example.entity.User;
import com.example.service.IncomeBillService;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@CrossOrigin
@RestController
@RequestMapping("/bill")
public class IncomeBillController {

    @GetMapping
    public Result getIncomeBill(@RequestParam String uname) {
        List<IncomeBill> list=incomeBillService.getIncomeBill(uname);
        return Result.success(list);
    }
    @Resource
    private IncomeBillService incomeBillService;

    @GetMapping("/search")
    public Result findBySearch(Parmas params) {
        PageInfo<IncomeBill> info = incomeBillService.findBySearch(params);
        return Result.success(info);
    }

    @PostMapping
    public Result save(@RequestBody IncomeBill incomeBill) {
        if(incomeBill.getId() == null) {
            incomeBillService.add(incomeBill);
        }else{
            incomeBillService.update(incomeBill);
        }
        return Result.success();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id){
        incomeBillService.delete(id);
        return Result.success();
    }

    @GetMapping("/echarts/billclassifybie")
    public Result billclassifybie(@RequestParam String uname){
        Parmas params = new Parmas();
        params.setUname(uname);
        List<IncomeBill> list =  incomeBillService.findAll(params);
        Map<String,Long> collect=list.stream().filter(x -> ObjectUtil.isNotEmpty(x.getBookname())).collect(Collectors.groupingBy(IncomeBill::getBookname,Collectors.counting()));

// 使用流处理数据，根据 billclassify 分组，并对 account 求和
        Map<String, Double> groupedSum = list.stream()
                .collect(Collectors.groupingBy(IncomeBill::getBillclassify, Collectors.summingDouble(IncomeBill::getAccount)));
        // 将结果转换为需要的格式
        List<Map<String, Object>> maplist = new ArrayList<>();
        for (String key : groupedSum.keySet()) {
            Map<String, Object> map = new HashMap<>();
            map.put("name", key);
            map.put("value", groupedSum.get(key));
            maplist.add(map);
        }
        return  Result.success(maplist);
    }



    @GetMapping("/echarts/billclassifybar")
    public Result billclassifybar(@RequestParam String uname){
        Parmas params = new Parmas();
        params.setUname(uname);
        List<IncomeBill> list =  incomeBillService.findAll(params);
            Map<String,Long> collect=list.stream().filter(x -> ObjectUtil.isNotEmpty(x.getBookname())).collect(Collectors.groupingBy(IncomeBill::getBookname,Collectors.counting()));
        // 使用流处理数据，分别计算支出和收入
        Map<String, Double> incomeMap = list.stream()
                .filter(bill -> "收入".equals(bill.getInoutclassify()))
                    .collect(Collectors.groupingBy(IncomeBill::getBillclassify, Collectors.summingDouble(IncomeBill::getAccount)));

        // 提取账本名称作为 x 轴数据
        List<String> xAxis = new ArrayList<>(incomeMap.keySet());
        // 提取总支出作为 y 轴数据
        List<Double> yAxis = new ArrayList<>(incomeMap.values());
        // 将数据封装成一个对象，返回给前端
        Map<String, Object> map = new HashMap<>();
        map.put("xAxis", xAxis);
        map.put("yAxis", yAxis);


        return  Result.success(map);
    }

}